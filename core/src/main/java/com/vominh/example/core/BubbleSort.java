package com.vominh.example.core;

import java.util.ArrayList;
import java.util.List;

public class BubbleSort {

	public static void main(String[] args) {
		int[] range1 = { 0, 1, 2, 3, 4, 5, 6 };
		int[] range2 = { 0, 1, 2, 3, 4, 5, 6, 7 };
		List<Example> list = new ArrayList<Example>();

		list.add(new Example(3));
		list.add(new Example(2));
		list.add(new Example(1));
		list.add(new Example(5));
		list.add(new Example(4));
		list.add(new Example(9));
		list.add(new Example(7));
		list.add(new Example(8));

		for (Example obj : list) {
			System.out.print(obj.getNumber() + " ");
		}

		System.out.println();

		for (int i : range1) {
			// System.out.print(l + " ");
			for (int j : range2) {
				 System.out.print(j + " ");
				if (list.get(i).getNumber() < list.get(j).getNumber()) {
					Example exa = list.get(i);
					list.set(i, list.get(j));
					list.set(j, exa);
				}

			}
			System.out.println();
		}

		System.out.println("after sort");
		for (Example obj : list) {
			System.out.print(obj.getNumber() + " ");
		}

	}

}

class Example {
	private int number;

	public Example() {
	}

	public Example(int number) {
		super();
		this.number = number;
	}

	public int getNumber() {
		return number;
	}

	public void setNumber(int number) {
		this.number = number;
	}

}
